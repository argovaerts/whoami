const equals = (a, b) => JSON.stringify(a) === JSON.stringify(b);

const konamiArray = [];
const konamiCode = ['ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'ArrowLeft', 'ArrowRight', 'b', 'a'];

const konami = () => {
    document.addEventListener('keydown', (event) => {
        const keyName = event.key;
        konamiArray.push(keyName);
        if(konamiArray.length > konamiCode.length) {
          konamiArray.shift();
        }
        console.log(konamiArray);
        if(equals(konamiArray, konamiCode)) {
          document.querySelector('#stylesheet').setAttribute('href', 'terminal.css');
          const audio = new Audio('crockett.mp3');
          audio.loop = true;
          audio.currentTime = 1;
          audio.play();

          const madonna = document.createElement('img');
          madonna.setAttribute('src', 'madonna.gif');
          madonna.classList.add('konami-image', 'konami-right');
          document.querySelector('header').insertAdjacentElement('afterend', madonna);

          const viceFists = document.createElement('img');
          viceFists.setAttribute('src', 'vice_fists.gif');
          viceFists.classList.add('konami-image', 'konami-left');
          const sections = [...document.querySelectorAll('section')];
          const section = sections.pop();
          document.body.insertBefore(viceFists, section);
          document.querySelector('#arne').classList.add('rotate-image');
        }
      }, false);
};

konami();